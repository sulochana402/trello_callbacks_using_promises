/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/
const callback1ThanosBoards = require("./callback1.cjs");
const callback2ThanosLists = require("./callback2.cjs");
const callback3MindAndSpaceListCards = require("./callback3.cjs");

function callback5() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        let mindListId, spaceListId;
  
        // Get Thanos boards information
        callback1ThanosBoards("mcu453ed")
          .then((boards) => {
            console.log("Thanos Board information:", boards);
            return callback2ThanosLists("mcu453ed");
          })
          .then((lists) => {
            console.log("Thanos List information:", lists);
            return callback3MindAndSpaceListCards("qwsa221")
          })
          .then((mindListCards) => {
            console.log("Cards for mind list:", mindListCards);
            return callback3MindAndSpaceListCards("jwkh245")
          })
          .then((spaceListCards) => {
            console.log("Cards for Space list:", spaceListCards);
            resolve();
          })
          .catch((error) => {
            console.error("Error:", error.message);
            reject(error);
          });
      }, 2000);
    });
  }
  
module.exports = callback5;
