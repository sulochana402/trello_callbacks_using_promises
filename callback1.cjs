/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/
const board = require("./boards.json");

function callback1(boardID) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      let boardInfo = board.find((boards) => {
        return boards.id === boardID;
      });
      if (boardInfo) {
        resolve(boardInfo);
      } else {
        reject("Board not found");
      }
    }, 2000);
  });
}
module.exports = callback1;
