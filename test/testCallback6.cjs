const callback6 = require("../callback6.cjs");

callback6('mcu453ed')
  .then(() => {
    console.log("All lists and cards fetched successfully!");
  })
  .catch((error) => {
    console.error("Error:", error);
  });
