const callback5 = require("../callback5.cjs");

callback5()
  .then(() => {
    console.log("All operations completed successfully");
  })
  .catch((error) => {
    console.error("Error:", error);
  });
