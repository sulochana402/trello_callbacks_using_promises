const callback1 = require("../callback1.cjs");

callback1("mcu453ed")
  .then((boardInfo) => {
    console.log("Board found:", boardInfo);
  })
  .catch((err) => {
    console.error("Error:", err);
  });
