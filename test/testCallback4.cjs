const callback4 = require("../callback4.cjs");

callback4("mcu453ed", "Mind")
  .then(() => {
    console.log("All operations completed successfully");
  })
  .catch((error) => {
    console.error("Error:", error);
  });
