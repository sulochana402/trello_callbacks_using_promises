const callback2 = require("../callback2.cjs");

callback2("mcu453ed")
  .then((boardInfo) => {
    console.log("lists Belonging To Board:", boardInfo);
  })
  .catch((err) => {
    console.error("Error:", err);
  });
