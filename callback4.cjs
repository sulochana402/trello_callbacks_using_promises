/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const callback1ThanosBords = require("./callback1.cjs");
const callback2ThanosLists = require("./callback2.cjs");
const callback3MindListCards = require("./callback3.cjs");

function callback4(thanosId, listName) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      callback1ThanosBords(thanosId)
        .then((thanosBoard) => {
          console.log("Thanos Board information:", thanosBoard);
          return callback2ThanosLists(thanosId);
        })
        .then((thanosList) => {
          console.log("Thanos List information:", thanosList);
          const mindList = thanosList.find((list)=>{
            return list.name === listName;
          });
          if (!mindList) {
            throw new Error("Mind list not found");
          }
          return callback3MindListCards(mindList.id);
        })
        .then((cards) => {
          console.log(" cards for Mind list:", cards);
          resolve();
        })
        .catch((err) => {
          console.error("Error:", err);
          reject(err);
        });
    }, 2000);
  });
}
module.exports = callback4;
