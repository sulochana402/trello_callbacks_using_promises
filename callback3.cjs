/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
const cards = require("./cards.json");

function callback3(listID) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (listID in cards) {
                resolve(cards[listID]);
            } else {
                reject("No cards found for the specified list");
            }
        }, 2000);
    });
}

module.exports = callback3;
