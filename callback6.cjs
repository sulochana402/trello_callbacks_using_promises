const callback1ThanosBoards = require("./callback1.cjs");
const callback2ThanosLists = require("./callback2.cjs");
const callbackAllListCards = require("./callback3.cjs");

function callback6(thanosId) {
  return new Promise((resolve, reject) => {
    callback1ThanosBoards(thanosId)
      .then((board) => {
        console.log("Thanos Board ID:", board);
        return callback2ThanosLists(thanosId);
      })
      .then((lists) => {
        console.log("Lists for Thanos Board:", lists);
        let listCardPromises = [];
        for (let list of lists) {
          listCardPromises.push(
            new Promise((resolve, reject) => {
              callbackAllListCards(list.id)
                .then((cards) => {
                  console.log("All list cards:", list.id, cards);
                  resolve();
                })
                .catch((err) => {
                  console.error(err);
                  reject(err);
                });
            })
          );
        }
        return Promise.all(listCardPromises);
      })
      .then(() => {
        resolve();
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
}

module.exports = callback6;
