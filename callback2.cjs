/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/
const lists = require("./lists.json");

function callback2(listID) {
  return new Promise((resolve, reject) => {
      setTimeout(() => {
          if (listID in lists) {
              resolve(lists[listID]);
          } else {
              reject("No cards found for the specified list");
          }
      }, 2000);
  });
}
module.exports = callback2;
